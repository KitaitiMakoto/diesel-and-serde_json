use chrono::NaiveDateTime;
use super::schema::*;

// Don't know how to embed Selector into selectors field
#[derive(Debug, QueryableByName)]
#[table_name = "annotations"]
pub struct Annotation {
    pub id: i32,
    pub motivation: String,
    pub target: String,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime
}

#[derive(Debug, QueryableByName)]
#[table_name = "selectors"]
pub struct Selector {
    pub id: i32,
    pub annotation_id: i32,
    pub type_: String,
    pub conformsTo: String,
    pub value: String,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime
}
