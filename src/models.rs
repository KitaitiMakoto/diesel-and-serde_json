use chrono::NaiveDateTime;
use super::schema::*;

#[derive(Clone, Debug, Identifiable, Queryable)]
pub struct Annotation {
    pub id: i32,
    pub motivation: String,
    pub target: String,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime
}

#[derive(Debug, Insertable)]
#[table_name="annotations"]
pub struct NewAnnotation<'a> {
    pub motivation: &'a str,
    pub target: &'a str
}

#[derive(Debug, Identifiable, Queryable, Associations)]
#[belongs_to(Annotation)]
pub struct Selector {
    pub id: i32,
    pub annotation_id: i32,
    pub type_: String,
    pub comformsTo: String,
    pub value: String,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime
}

#[derive(Debug, Insertable)]
#[table_name="selectors"]
pub struct NewSelector<'a> {
    pub annotation_id: &'a i32,
    pub type_: &'a str,
    pub conformsTo: &'a str,
    pub value: &'a str
}
