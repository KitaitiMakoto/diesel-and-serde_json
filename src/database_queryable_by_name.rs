use diesel::prelude::*;
use diesel::sql_query;
use std::env;
use models_queryable_by_name::*;

pub fn retrieve_annotations(conn: &SqliteConnection) -> Vec<Annotation> {
    use super::schema::selectors::dsl::*;

    sql_query("SELECT * FROM annotations")
        .load::<Annotation>(conn)
        .expect("Error loading annotations")
}

pub fn retrieve_selectors(conn: &SqliteConnection) -> Vec<Selector> {
    use super::schema::selectors::dsl::*;

    sql_query("SELECT id, annotation_id, type as type_, conformsTo, value, created, modified FROM selectors")
        .load::<Selector>(conn)
        .expect("Error loading selectors")
}
