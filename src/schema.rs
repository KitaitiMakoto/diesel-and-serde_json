table! {
    annotations (id) {
        id -> Integer,
        motivation -> Text,
        target -> Text,
        created -> Timestamp,
        modified -> Timestamp,
    }
}

table! {
    selectors (id) {
        id -> Integer,
        annotation_id -> Integer,
        #[sql_name = "type"]
        type_ -> Text,
        conformsTo -> Text,
        value -> Text,
        created -> Timestamp,
        modified -> Timestamp,
    }
}

joinable!(selectors -> annotations (annotation_id));

allow_tables_to_appear_in_same_query!(
    annotations,
    selectors,
);
