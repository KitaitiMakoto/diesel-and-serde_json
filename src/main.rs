#[macro_use]
extern crate diesel;
extern crate chrono;

mod models;
mod schema;
mod database;

mod models_queryable_by_name;
mod database_queryable_by_name;

fn main() {
    run_with_queryable();
    run_with_queryable_by_name();
}

fn run_with_queryable() {
    println!("=== Run with Queryable ===");

    let conn = database::establish_connection();

    database::create_annotation(&conn, "bookmarking", "https://epub.directory/9784873117430/2015-11-18t15:53:01z");
    let anno_id = database::retrieve_latest_annotation_id(&conn);
    database::create_selector(&conn, &anno_id, "FragmentSelector", "http://www.idpf.org/epub/linking/cfi/epub-cfi.html", "epubcfi(/6/4[chap01ref]!/4[body01]/10[para05]/3:10)");

    let annos = database::retrieve_annotations(&conn);

    for anno in annos {
        let selectors = database::retrieve_selectors_belonging_to_annotation(&conn, &anno);
        println!("{:?}", anno);
        println!("{:?}", selectors);
    }
}

fn run_with_queryable_by_name() {
    println!("=== Run with QueryableByName ===");

    let conn = database::establish_connection();

    let annotations = database_queryable_by_name::retrieve_annotations(&conn);
    println!("{:?}", annotations);

    let selectors = database_queryable_by_name::retrieve_selectors(&conn);

    println!("{:?}", selectors);
}
