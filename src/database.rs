use diesel::prelude::*;
use std::env;
use models::*;

pub fn establish_connection() -> SqliteConnection {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn retrieve_annotations(conn: &SqliteConnection) -> Vec<Annotation> {
    use super::schema::annotations::dsl::*;

    annotations
        .load::<Annotation>(conn)
        .expect("Error loading annotations")
}

pub fn retrieve_latest_annotation_id(conn: &SqliteConnection) -> i32 {
    use super::schema::annotations::dsl::*;

    let annos = annotations
        .select(id)
        .order(id.desc())
        .limit(1)
        .load::<i32>(conn)
        .expect("Error loading annotation");
    annos[0]
}

pub fn create_annotation(conn: &SqliteConnection, motivation: &str, target: &str) -> usize {
    use schema::annotations;

    let new_anno = NewAnnotation {
        motivation: motivation,
        target: target
    };

    diesel::insert_into(annotations::table)
        .values(&new_anno)
        .execute(conn)
        .expect("Error saving new annotation")
}

pub fn retrieve_selectors_belonging_to_annotation(conn: &SqliteConnection, annotation: &Annotation) -> Vec<Selector> {
    use super::schema::selectors::dsl::*;

    Selector::belonging_to(annotation)
        .get_results::<Selector>(conn)
        .expect("Error loading selectors")
}

pub fn create_selector(conn: &SqliteConnection, annotation_id: &i32, type_: &str, conformsTo: &str, value: &str) -> usize {
    use schema::selectors;

    let new_selector = NewSelector {
        annotation_id: annotation_id,
        type_: type_,
        conformsTo: conformsTo,
        value: value
    };

    diesel::insert_into(selectors::table)
        .values(&new_selector)
        .execute(conn)
        .expect("Error saving new selector")
}
